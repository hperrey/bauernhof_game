# Marlene und Justus auf dem Bauernhof
## Spiel vorbereiten
- Python installieren: <https://www.python.org>
- PyGame installieren: <https://www.pygame.org>
- PGZero installieren: <https://www.pygame-zero.readthedocs.io>

Sobald Python vorhanden ist, können die Bibliotheken typischerweise mit einem einfachen `pip install pgzero` installiert werden.

Detailierte Instruktionen gibt es unter <https://www.pygame.org/wiki/GettingStarted>

Getestet wurde das Spiel mit Python 3.7, PyGame 1.9.6 und PyGame-Zero 1.2.

## Spiel starten

``` sh
python3 bauernhof.py
```

## Bedienung
Im Menu kann mit `f` in den Vollbildmodus gewechselt werden. `Leertaste` startet das Spiel und `q` order `Escape` beenden das Spiel.

Gesteuert wird der Hase mit den Cursor-Tasten. Ziel ist es, zu einem der Bauernhof-Tiere am Ende des Levels zu kommen. Dabei müssen Hindernisse wie Autos und Züge vermieden und Flüsse überwunden werden. Mit jedem gefundenen Tier werden die Level länger.

## Verantwortliche
Musik & Zeichnungen: Rebecka Ostréus
Coding & Einfärbung: Hanno Perrey

Basierend auf "Infinite Bunner".

## Lizenz
Basiert auf "Infinite Bunner" aus "Code the Classics". Lizenz: CC BY-NC-SA 3.0
Die verwendeten Fonts sind unter SIL OPEN FONT LICENSE Version 1.1 lizensiert.
